<?php

namespace CodeceptBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;

class ParseCodeceptResults
{
    public $xml;
    public $product;
    public $url = "<file>.xml";

    public function __construct()
    {
        $this->setProductName();
    }

    protected function configure()
    {   
        $productName = 'sentinel';
        $url = 100;

        $this->setName("codeceptconsole:parseresults")
             ->setDescription("Parse codeception xml build results and add those to a database")
             ->setDefinition(array(
                      new InputOption('product', 'cp', InputOption::VALUE_OPTIONAL, 'Product name.', $start),
                      new InputOption('url', 'cu', InputOption::VALUE_OPTIONAL, 'Url to the codeception build results xml file.', $stop)
                ))
             ->setHelp(<<<EOT
Display the fibonacci numbers between a range of numbers given as parameters

Usage:

<info>php console.php phpmaster:fibonacci 2 18 <env></info>

You can also specify just a number and by default the start number will be 0
<info>php console.php phpmaster:fibonacci 18 <env></info>

If you don't specify a start and a stop number it will set by default [0,100]
<info>php console.php phpmaster:fibonacci<env></info>
EOT
);
    }

    public function loadFile()
    {
        $this->xml = simplexml_load_file($this->url);
    }

    public function mutateToArray()
    {
        $json_string = json_encode($this->xml);
        return json_decode($json_string, TRUE);
    }

    public function setProductName()
    {
        $product = explode("/", $this->url);
        $this->product = $product[4];
    }
}

class FormatOutput
{
    public static function getPathToTest($string = '')
    {
        $string = $string ? $string : 'PathToTest.testSanityCheck';
        $patterns = array();
        $patterns[0] = '/^Sentry\./';
        $patterns[1] = '/\.testSanityCheck/';
        $patterns[2] = '/\./';
        $patterns[3] = '/Cests/';
        $replacements = array();
        $replacements[0] = 'src/Sentry/';
        $replacements[1] = '';
        $replacements[2] = '/';
        $replacements[3] = 'Cest'; // hack
        // return 'php runCodecept.php -s ' . preg_replace($patterns, $replacements, $string) . '.php';
        return preg_replace($patterns, $replacements, $string) . '.php';
    }
}

$obj = new \codeception\acceptance\LoadFileParseResults();
$obj->loadFile();

echo "Product {$obj->product} Type Test {$obj->xml->testsuite->attributes()->name}\n";
echo "Total Build Time {$obj->xml->testsuite->attributes()->time}\n";
echo "Tests {$obj->xml->testsuite->attributes()->tests}\n";
echo "Assertions {$obj->xml->testsuite->attributes()->assertions}\n";
echo "Failures {$obj->xml->testsuite->attributes()->failures}\n";
echo "Errors {$obj->xml->testsuite->attributes()->errors}\n\n";

$errors = array();
$failures = array();
$testFilesError = array();
$testFilesFailed = array();

foreach($obj->xml->testsuite as $x) {
    foreach($x as $i) {
        if(array_key_exists('error', $i)) {
            $testFilesError[] = FormatOutput::getPathToTest($i->attributes()->file);
            echo "Total Test Time {$i->attributes()->time}\n";
            echo "Type Error  {$i->error->attributes()->type}\n";
            echo "Test File {$i->attributes()->file}\n";
            $errors[] =  "{$i->error}\n";
        }
        if(array_key_exists('failure', $i)) {
            $testFilesFailed[] = FormatOutput::getPathToTest($i->attributes()->file);
            echo "Total Test Time {$i->attributes()->time}\n";
            echo "Test File {$i->attributes()->file}\n";
            $failures[] = "{$i->failure}\n";
        }
    }
}

echo "\n\nError Messages\n\n";
foreach($errors as $e) {
    echo $e;
}

echo "\n\nFailed Messages\n\n";
foreach($failures as $f) {
    echo $f;
}

if($testFilesError) {
    echo "\n\n" . count($testFilesError) . " Failed Tests with Errors that need to be Re-Run\n\n";
    foreach($testFilesError as $aFile) {
        echo "$aFile\n";
    }
}

if($testFilesFailed) {
    echo "\n\n" . count($testFilesFailed) . " Failed Tests that need to be Re-Run\n\n";
    foreach($testFilesFailed as $bFile) {
        echo "$bFile\n";
    }
}

$copyFiles = 1;

if($copyFiles) {
    // This must be run from the ~/<repository> repository or codeception branch directory
    $structure = './<src/File>/' . ucfirst($obj->product) . 'Bundle/Tests/codeception/acceptance/' . ucfirst($obj->product) . 'FailedTestsSuite';

    // To create the nested structure, the $recursive parameter 
    // to mkdir() must be specified.

    if(!is_dir($structure)) {
        if (!mkdir($structure, 0777, true)) {
            die('Failed to create folders...');
        }
    }

    $allFailedTests = array_merge($testFilesError, $testFilesFailed);

    $i = 0;
    foreach($allFailedTests as $aFailedTest) {
        $aTestCopy = explode("/", $aFailedTest);
        $bTestCopy = $aTestCopy[(count($aTestCopy) - 1)];
        if(is_file("./{$aFailedTest}")) {
            if (!copy("./{$aFailedTest}", "{$structure}/" . ++$i . $bTestCopy)) {
                die('Failed to copy test...');
            }
        } else {
            echo "\n\n OOPs, this test failed, however, this test file does not exist\n ./{$aFailedTest}\n";
        }
    }
}


class CodeceptCommand extends Command {






    protected function configure()
    {   
        $start = 0;
        $stop = 100;

        $this->setName("codeceptconsole:fibonacci")
             ->setDescription("Display the fibonacci numbers between 2 given numbers")
             ->setDefinition(array(
                      new InputOption('start', 'as', InputOption::VALUE_OPTIONAL, 'Start number of the range of Fibonacci number', $start),
                      new InputOption('stop', 'ae', InputOption::VALUE_OPTIONAL, 'stop number of the range of Fibonacci number', $stop)
                ))
             ->setHelp(<<<EOT
Display the fibonacci numbers between a range of numbers given as parameters

Usage:

<info>php console.php phpmaster:fibonacci 2 18 <env></info>

You can also specify just a number and by default the start number will be 0
<info>php console.php phpmaster:fibonacci 18 <env></info>

If you don't specify a start and a stop number it will set by default [0,100]
<info>php console.php phpmaster:fibonacci<env></info>
EOT
);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $header_style = new OutputFormatterStyle('white', 'green', array('bold'));
        $output->getFormatter()->setStyle('header', $header_style);

        $start = intval($input->getOption('start'));
        $stop  = intval($input->getOption('stop'));

        if ( ($start >= $stop) || ($start < 0) ) {
           throw new \InvalidArgumentException('Stop number should be greater than start number');
        }

        $output->writeln('<header>Fibonacci numbers between '.$start.' - '.$stop.'</header>');

        $xnM2 = 0; // set x(n-2)
        $xnM1 = 1;  // set x(n-1)
        $xn = 0; // set x(n)
        $totalFiboNr = 0;
        while ($xnM2 <= $stop)
        {
            if($xnM2 >= $start)  {
                $output->writeln('<header>'.$xnM2.'</header>');
                $totalFiboNr++;
            }
            $xn = $xnM1 + $xnM2;
            $xnM2 = $xnM1;
            $xnM1 = $xn;

        }
        $output->writeln('<header>Total of Fibonacci numbers found = '.$totalFiboNr.' </header>');
    }
}

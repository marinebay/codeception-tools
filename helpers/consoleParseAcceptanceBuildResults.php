<?php
namespace codeception\acceptance;

class LoadFileParseResults
{
    public $xml;
    public $product = "datanex";
    public $url = "http://selenium7ie11.sentryds.com/codeception/datanex/acceptance/2015/2015-11-16/20151116083231.report.xml";

    public function loadFile()
    {
        $this->xml = simplexml_load_file($this->url);
    }

    public function mutateToArray()
    {
        $json_string = json_encode($this->xml);
        return json_decode($json_string, TRUE);
    }   
}

$obj = new \codeception\acceptance\LoadFileParseResults();
$obj->loadFile();

echo "Product {$obj->product} Type Test {$obj->xml->testsuite->attributes()->name}\n";
echo "Total Build Time {$obj->xml->testsuite->attributes()->time}\n";
echo "Tests {$obj->xml->testsuite->attributes()->tests}\n";
echo "Assertions {$obj->xml->testsuite->attributes()->assertions}\n";
echo "Failures {$obj->xml->testsuite->attributes()->failures}\n";
echo "Errors {$obj->xml->testsuite->attributes()->errors}\n\n";

$errors = array();
$failures = array();

foreach($obj->xml->testsuite as $x) {
    foreach($x as $i) {
        if(array_key_exists('error', $i)) {
            echo "Total Test Time {$i->attributes()->time}\n";
            echo "Type Error  {$i->error->attributes()->type}\n";
            echo "Test File {$i->attributes()->file}\n";
            $errors[] =  "{$i->error}\n";
        }
        if(array_key_exists('failure', $i)) {
            echo "Total Test Time {$i->attributes()->time}\n";
            echo "Test File {$i->attributes()->file}\n";
            $failures[] = "{$i->failure}\n";
        }
    }
}

echo "\n\nError Messages\n\n";
foreach($errors as $e) {
    echo $e;
}

echo "\n\nFailed Messages\n\n";
foreach($failures as $f) {
    echo $f;
}

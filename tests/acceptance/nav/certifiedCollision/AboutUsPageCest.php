<?php
class AboutUsPageCest 
{
    public function _before(\AcceptanceTester $I)
    {
    }

    public function _after(\AcceptanceTester $I)
    {
    }

    // tests
    public function tryToTest(\AcceptanceTester $I) 
    {    
        $I->wantTo('visit the Certified Collision of Stuart about us page');
        $I->amOnPage('/');
        $I->waitForText('Family owned and operated since 1991!', 60, "i");
        $I->waitForElement('ul.nav.nav-justified li.page_item.page-item-2.current_page_item a');
        $I->click('ABOUT');
        $I->waitForText('About Certified Collision Experts, LLC.', 60, 'h1.main-title.align-center');
    }
}

<?php
class HomePageCest 
{
    public function _before(\AcceptanceTester $I)
    {
    }

    public function _after(\AcceptanceTester $I)
    {
    }

    // tests
    public function tryToTest(\AcceptanceTester $I) 
    {    
        $I->wantTo('visit the Certified Collision of Stuart home page');
        $I->amOnPage('/');
        $I->waitForText('Family owned and operated since 1991!', 60, "i");
        $I->waitForElement('ul.nav.nav-justified li.page_item.page-item-2.current_page_item a');
        $I->waitForElement('/html/body/div[1]/div/div[1]/ul/li[2]/a');
        $I->waitForElement('/html/body/div[1]/div/div[1]/ul/li[3]/a');
        $I->waitForText('2015 by Certified Collision Experts, LLC. All rights reserved.', 60, 'copyright');
    }
}
